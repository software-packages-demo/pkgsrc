# pkgsrc

Building third-party software on NetBSD and other UNIX-like systems. https://pkgsrc.org

# Source packages index
* https://pkgsrc.se/

# Source code
* https://github.com/NetBSD/pkgsrc
* [pkgdev:building](https://github.com/joyent/pkgsrc/wiki/pkgdev:building)

# WikipediA
* [*pkgsrc*](https://en.m.wikipedia.org/wiki/Pkgsrc)
* [*NetBSD*](https://en.m.wikipedia.org/wiki/NetBSD)

# Using pkgsrc on systems other than NetBSD
* [*How to use pkgsrc on Linux*
  ](https://opensource.com/article/19/11/pkgsrc-netbsd-linux)
  2019-11 Seth Kenlon (Red Hat)

## Included in distributions
* [pkgsrc](https://repology.org/projects/?search=pkgsrc) (Repology)

### Arch Linux AUR
* [netbsd-pkgsrc](https://aur.archlinux.org/packages/netbsd-pkgsrc)

## Bootstrapping
* [*Bootstrapping pkgsrc*
  ](http://www.netbsd.org/docs/pkgsrc/platforms.html#bootstrapping-pkgsrc)
* [*How to use pkgsrc on linux*
  ](http://wiki.netbsd.org/pkgsrc/how_to_use_pkgsrc_on_linux/)
* https://github.com/Nardberjean/travis-pkgsrc-demo/blob/master/.travis.yml

## Install binary packages (SmartOS/illumos, Mac OS X, Linux)
* [*Choose Your Linux Package Set*
  ](http://pkgsrc.joyent.com/install-on-linux/)
  * http://pkgsrc.joyent.com